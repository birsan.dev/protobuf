import com.google.protobuf.InvalidProtocolBufferException;
import generated.PersonProto;
import model.Person;

import java.util.Arrays;

public class PersonDemo {

    private static Person createPerson() {
        return new Person()
                .setName("Mike")
                .setAge(22)
                .setRelatives(Arrays.asList("Alex, George, Theo"));
    }

    private static Person createPersonFromBinary(byte[] data) throws InvalidProtocolBufferException {
        PersonProto.Person generatedPerson = PersonProto.Person.parseFrom(data);
        return new Person().setName(generatedPerson.getName())
                .setAge(generatedPerson.getAge())
                .setRelatives(generatedPerson.getRelativesList());
    }

    public static void main(String... args) throws InvalidProtocolBufferException {
        Person person = createPerson();
        PersonProto.Person personMessage = PersonProto.Person.newBuilder()
                .setName(person.getName())
                .setAge(person.getAge())
                .addAllRelatives(person.getRelatives())
                .build();

        Person personFromBinary = createPersonFromBinary(personMessage.toByteArray());

        System.out.println(person.equals(personFromBinary));

    }

}
