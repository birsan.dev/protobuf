package model;

import java.util.List;
import java.util.Objects;

public class Person {

    private String name;
    private int age;
    private List<String> relatives;

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }

    public List<String> getRelatives() {
        return relatives;
    }

    public Person setRelatives(List<String> relatives) {
        this.relatives = relatives;
        return this;
    }

    @Override
    public String toString() {
        String allRelatives = String.join(",", relatives);
        return "name=" + name + ", age=" + age + ", relatives=" + allRelatives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(relatives, person.relatives);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, relatives);
    }
}
